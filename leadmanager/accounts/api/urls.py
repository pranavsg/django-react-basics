from django.urls import path, include

from knox import views as knox_views

from .views import RegisterAPIView, LoginAPIView, GetUserAPIView


app_name = "accounts-api"

urlpatterns = [
    # path('api/auth/', include('knox.urls')),
    path('api/auth/register/', RegisterAPIView.as_view(), name='register'),
    path('api/auth/login/', LoginAPIView.as_view(), name='login'),
    path('api/auth/user/', GetUserAPIView.as_view(), name='user'),
    path('api/auth/logout/', knox_views.LogoutView.as_view(), name='logout'),
]

# knox_views.LogoutView -> destroys the token on backend

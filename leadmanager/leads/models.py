from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Lead(models.Model):
    owner = models.ForeignKey(User, related_name="leads", on_delete=models.CASCADE,
        null=True)

    name = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
    message = models.TextField(blank=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s - %s" % (self.owner, self.email)

    class Meta:
        ordering = ('-id',)

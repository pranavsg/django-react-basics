from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string

from leads.models import Lead


class Command(BaseCommand):

    def handle(self, *args, **options):
        for i in range(3):
            lead, created = Lead.objects.get_or_create(
                name=get_random_string(length=6),
                email=f"{get_random_string(length=5)}@{get_random_string(length=5)}.com",
                message=get_random_string(length=50)
            )
            print(lead)

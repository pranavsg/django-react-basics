from rest_framework import routers

from .views import LeadViewSet

app_name = 'leads-api'

router = routers.DefaultRouter()
router.register('api/leads', LeadViewSet, 'leads-api')

urlpatterns = router.urls

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .serializers import LeadModelSerializer
from leads.models import Lead


class LeadViewSet(viewsets.ModelViewSet):
    serializer_class = LeadModelSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Lead.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

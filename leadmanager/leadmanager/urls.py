from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += [
    path('', include('frontend.urls', namespace='frontend')),
]

# apis
urlpatterns += [
    path('', include('leads.api.urls', namespace='leads-api')),
    path('', include('accounts.api.urls', namespace='accounts-api')),
]

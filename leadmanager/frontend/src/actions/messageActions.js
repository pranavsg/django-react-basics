import { CREATE_MESSAGE, GET_ERRORS } from './Types';

// CREATE MESSAGE
export const createMessage = message => {
  return {
    type: CREATE_MESSAGE,
    payload: message
  }
}

// RETURN MESSAGES
export const returnMessage = (message, status) => {
  return {
    type: GET_ERRORS,
    payload: { message, status }
  }
}

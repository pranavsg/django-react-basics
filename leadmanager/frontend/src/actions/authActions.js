import axios from 'axios';
import Cookies from 'js-cookie';

import { returnMessage } from './messageActions';
import {
  USER_LOADED,
  USER_LOADING,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL
} from './Types';

// SETUP CONFIG WITH TOKEN - helper function
export const tokenConfig = getState => {
  // GET TOKEN FROM STATE
  const token = getState().auth.token

  // HEADERS
  const config = {
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  // IF TOKEN ADD TO HEADERS
  if (token) {
    config['headers']['Authorization'] = `Token ${token}`;
  }

  return config;
}

// CHECK TOKEN AND LOAD USER
export const loadUser = () => (dispatch, getState) => {
  // USER LOADING
  dispatch({ type: USER_LOADING });

  // API REQUEST
  axios.get('/api/auth/user/', tokenConfig(getState))
    .then(res => {
      dispatch({
        type: USER_LOADED,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({ type: AUTH_ERROR })
      dispatch(returnMessage(err.response.data, err.response.status));
    })
}

// LOGIN USER
export const loginUser = (username, password) => dispatch => {
  // HEADERS
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  // REQUEST DATA
  const body = JSON.stringify({ username, password });

  // API REQUEST
  axios.post('/api/auth/login/', body, config)
    .then(res => (
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data
      })
    ))
    .catch(err => {
      dispatch({ type: LOGIN_FAIL });
      dispatch(returnMessage(err.response.data, err.response.status));
    })
}

// LOGOUT USER
export const logoutUser = () => (dispatch, getState) => {
  // API REQUEST
  axios.post('/api/auth/logout/', null, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: LOGOUT_SUCCESS,
      });
    })
    .catch(err => {
      dispatch(returnMessage(err.response.data, err.response.status));
    })
}

// REGISTER USER
export const registerUser = ({ username, password, email }) => dispatch => {
  // HEADERS
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  // REQUEST DATA
  const body = JSON.stringify({ username, email, password });

  // API REQUEST
  axios.post('/api/auth/register/', body, config)
    .then(res => (
      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data
      })
    ))
    .catch(err => {
      dispatch({ type: REGISTER_FAIL });
      dispatch(returnMessage(err.response.data, err.response.status));
    })
}

import axios from 'axios';

import { GET_LEADS, DELETE_LEAD, CREATE_LEAD } from './Types';
import { createMessage, returnMessage } from './messageActions';
import { tokenConfig } from './authActions';

// GET LEADS
export const getLeads = () => (dispatch, getState) => {
  axios.get('/api/leads/', tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_LEADS,
        payload: {
          items: res.data
        }
      })
    })
    .catch(err => dispatch(returnMessage(err.response.data, err.response.status)));
}

// DELETE LEADS
export const deleteLead = id => (dispatch, getState) => {
  axios.delete(`/api/leads/${id}/`, tokenConfig(getState))
    .then(res => {
      dispatch(createMessage({ leadDeleted: 'Lead deleted' }));
      dispatch({
        type: DELETE_LEAD,
        payload: id
      })
    })
    .catch(err => console.error(err));
}

// CREATE LEAD
export const createLead = (name, email, message) => (dispatch, getState) => {
  const lead = {
    name, email, message
  };

  axios.post('/api/leads/', JSON.stringify(lead), tokenConfig(getState))
    .then(res => {
      dispatch(createMessage({ leadAdded: 'Lead addded' }));
      dispatch({
        type: CREATE_LEAD,
        payload: res.data
      })
    })
    .catch(err => {
      dispatch(returnMessage(err.response.data, err.response.status));
    })
    // err.response.data gives the exact error messages
    // err.response.status for response status code
}

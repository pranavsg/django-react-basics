export const GET_LEADS = 'leads:GET_LEADS';

export const DELETE_LEAD = 'leads:DELETE_LEAD';

export const CREATE_LEAD = 'leads:CREATE_LEAD';

export const GET_ERRORS = 'errors:GET_ERRORS';

export const CREATE_MESSAGE = 'messages:CREATE_MESSAGE';

export const USER_LOADING = 'auth:USER_LOADING';

export const USER_LOADED = 'auth:USER_LOADED';

export const AUTH_ERROR = 'auth:AUTH_ERROR';

export const LOGIN_SUCCESS = 'auth:LOGIN_SUCCESS';

export const LOGIN_FAIL = 'auth:LOGIN_FAIL';

export const LOGOUT_SUCCESS = 'auth:LOGOUT_SUCCESS';

export const REGISTER_SUCCESS = 'auth:REGISTER_SUCCESS';

export const REGISTER_FAIL = 'auth:REGISTER_FAIL';

import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { registerUser } from '../../actions/authActions';
import { createMessage } from '../../actions/messageActions';

class Register extends Component {
  state = {
    username: '',
    email: '',
    password: '',
    password2: ''
  }

  handleSubmit = e => {
    e.preventDefault();
    const { username, email, password, password2 } = this.state;

    // CHECK IF PASSWORDS MATCH
    if (password !== password2) {
      this.props.createMessage({ passwordsNotMatch: 'Passwords do not match.' })
    } else {
      this.props.registerUser({
        username, email, password
      })
    }
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/dashboard" />
    }

    const { username, email, password, password2 } = this.state;

    return (
      <div className="row mt-5">
        <div className="col-md-6 offset-md-3">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Register</h5>
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label htmlFor="idName">Username</label>
                  <input
                    id="idUsername"
                    name="username"
                    type="text"
                    value={username}
                    onChange={this.handleChange}
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="idName">Email</label>
                  <input
                    id="idEmail"
                    name="email"
                    type="email"
                    value={email}
                    onChange={this.handleChange}
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="idName">Password</label>
                  <input
                    id="idPassword"
                    name="password"
                    type="password"
                    value={password}
                    onChange={this.handleChange}
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="idName">Confirm Password</label>
                  <input
                    id="idPassword2"
                    name="password2"
                    type="password"
                    value={password2}
                    onChange={this.handleChange}
                    className="form-control"
                  />
                </div>
                <button type="submit"
                  className="btn btn-success font-weight-bold"
                >Register</button>
                <p>Already have an account? <Link to="/login">Login</Link></p>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = {
  registerUser, createMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);

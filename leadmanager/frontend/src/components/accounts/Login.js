import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { loginUser } from '../../actions/authActions';

class Login extends Component {
  state = {
    username: '',
    password: ''
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.onLoginUser(this.state.username, this.state.password);
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  render() {
    if (this.props.auth.isAuthenticated) {
      return <Redirect to="/dashboard" />
    }

    const { username, password } = this.state;

    return (
      <div className="row mt-5">
        <div className="col-md-6 offset-md-3">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Login</h5>
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label htmlFor="idName">Username</label>
                  <input
                    id="idUsername"
                    name="username"
                    type="text"
                    value={username}
                    onChange={this.handleChange}
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="idName">Password</label>
                  <input
                    id="idPassword"
                    name="password"
                    type="password"
                    value={password}
                    onChange={this.handleChange}
                    className="form-control"
                  />
                </div>
                <button type="submit"
                  className="btn btn-success font-weight-bold"
                >Login</button>
                <p>Don't have an account? <Link to="/register">Register</Link></p>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = {
  onLoginUser: loginUser
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

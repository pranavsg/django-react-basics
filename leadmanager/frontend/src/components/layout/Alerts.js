import React, { Component, Fragment } from 'react';
import { withAlert } from 'react-alert';
import { connect } from 'react-redux';

class Alerts extends Component {
  componentDidUpdate(prevProps) {
    const { errors, alert, messages } = this.props;
    if (prevProps.errors !== errors) {
      if (errors.message.name) alert.error(
        `Name: ${errors.message.name.join()}`
      );
      if (errors.message.email) alert.error(
        `Email: ${errors.message.email.join()}`
      );
      if (errors.message.message) alert.error(
        `Message: ${errors.message.message.join()}`
      );
      if (errors.message.username) alert.error(
        `Username: ${errors.message.username.join()}`
      );
      if (errors.message.non_field_errors) alert.error(
        (errors.message.non_field_errors.join())
      );
    }

    if (prevProps.messages !== messages) {
      if (messages.leadDeleted) alert.success(messages.leadDeleted);
      if (messages.leadAdded) alert.success(messages.leadAdded);
      if (messages.passwordsNotMatch) alert.error(messages.passwordsNotMatch);
    }
  }

  render() {
    return <Fragment />
  }
}

const mapStateToProps = state => ({
  errors: state.errors,
  messages: state.messages
});

export default connect(mapStateToProps)(withAlert()(Alerts));

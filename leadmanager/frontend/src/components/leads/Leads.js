import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import { getLeads, deleteLead } from '../../actions/leadsActions';

export class Leads extends Component {
  componentDidMount = e => {
    this.props.getLeads();
  }

  render () {
    return (
      <Fragment>
        <h1>Leads</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>Message</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {this.props.leads.items.map(item => (
              <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>{item.message}</td>
                <td>
                  <button
                    className="btn btn-sm btn-danger"
                    onClick={() => this.props.deleteLead(item.id)}
                    >Delete</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  leads: state.leads
});

const mapDispatchToProps = {
  getLeads,
  deleteLead
}

export default connect(mapStateToProps, mapDispatchToProps)(Leads);

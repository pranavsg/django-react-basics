import React, { Component } from 'react';
import { connect } from 'react-redux';

import { createLead } from '../../actions/leadsActions';

export class Form extends Component {
  state = {
    name: '',
    email: '',
    message: ''
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit = e => {
    e.preventDefault();
    const { name, email, message } = this.state;
    this.props.createLead(name, email, message);
    this.setState({
      name: '',
      email: '',
      message: ''
    })
  }

  render () {
    const { name, email, message } = this.state;
    return (
      <div className="card my-5">
        <div className="card-body">
          <h2 className="card-title">Add Lead</h2>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="idName">Name</label>
              <input
                id="idName"
                name="name"
                type="text"
                value={name}
                onChange={this.handleChange}
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label htmlFor="idName">Email</label>
              <input
                id="idEmail"
                name="email"
                type="email"
                value={email}
                onChange={this.handleChange}
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label htmlFor="idName">Message</label>
              <textarea
                id="idMessage"
                name="message"
                type="text"
                value={message}
                onChange={this.handleChange}
                className="form-control"
              />
            </div>
            <button type="submit"
              className="btn btn-success font-weight-bold"
            >Save</button>
          </form>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  leads: state.leads
});

const mapDispatchToProps = {
  createLead
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);

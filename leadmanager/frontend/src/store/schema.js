const schema = {
  leads: {
    items: []
  },
  errors: {
    message: null,
    status: null
  },
  messages: {},

  // auth initial state
  auth: {
    token: localStorage.getItem('token'),
    isAuthenticated: null,
    isLoading: false,
    user: null
  }
};

export default schema;

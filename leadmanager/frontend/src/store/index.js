import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import rootReducer from '../reducers';
import schema from './schema';

const middlewares = [thunk];

const store = createStore(
  rootReducer,
  schema,
  composeWithDevTools(
    applyMiddleware(...middlewares)
  )
);

export default store;

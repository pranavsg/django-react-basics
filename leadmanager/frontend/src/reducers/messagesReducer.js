import { CREATE_MESSAGE } from '../actions/Types';

const messagesReducer = (state = {}, { type, payload }) => {
  switch(type) {
    case CREATE_MESSAGE:
      return (state = payload); // set state to the message
    default:
      return state;
  }
};

export default messagesReducer;

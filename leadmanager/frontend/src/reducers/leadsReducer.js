import { GET_LEADS, DELETE_LEAD, CREATE_LEAD } from '../actions/Types';

const getLeads = (state = {}, { type, payload }) => {
  switch(type) {
    case GET_LEADS:
      return {
        ...state,  // return whatever else is in the state
        items: payload.items
      }
    case DELETE_LEAD:
      return {
        ...state,
        items: state.items.filter(item => item.id !== payload)
      }
    case CREATE_LEAD:
      // state.items.unshift(payload)
      return {
        ...state,
        items: [payload, ...state.items]
      }
    default:
      return state;
  }
};

export default getLeads;

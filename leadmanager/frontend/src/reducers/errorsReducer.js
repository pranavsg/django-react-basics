import { GET_ERRORS } from '../actions/Types';

const errorsReducer = (state = {}, { type, payload }) => {
  switch(type) {
    case GET_ERRORS:
      return {
        message: payload.message,
        status: payload.status
      }
    default:
      return state;
  }
};

export default errorsReducer;

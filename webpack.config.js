module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,  // for any js file
                exclude: /node_modules/, // exclude node_modules folder
                use: {
                    loader: "babel-loader"  // use babel to transpile our code
                }
            }
        ]
    }
}
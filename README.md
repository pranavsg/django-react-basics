# Initialization

**NOTE: We will not be using `npx create-react-app my-app` for integrating react in our django project, we will be creating react app from scratch using babel & webpack.**

**Before creating django project, please run the below commands.**
```
# on windows, create virtualenv using (You can use pipenv on linux)
python -m venv env
# activate virtualenv
cd env/scripts
activate
# install packages
pip install django django-rest-framework django-rest-knox
```

1. Create django project.
    `django-admin startproject leadmanager`

2. Create leads app.
    `python manage.py startapp leads`

3. Create frontend app.
    `python manage.py startapp frontend`

4. Create subfolders in the frontend app for our react app.
    All our **react app** files will exist in the **frontend app**
    Create subfolders using,

    ```
    # on cmd from django project root
    mkdir frontend\src\components
    mkdir frontend\static\frontend
    mkdir frontend\templates\frontend
    ```

    ```
    # on terminal from django project root
    mkdir -p frontend\src\components
    mkdir -p frontend\{static,templates}\frontend
    ```

    **frontend\src\components** will have all our react app files
    **frontend\static\frontend** will have react build files
    **frontend\templates\frontend** will have our entry index.html file

5. Outside the django project root, create npm project using,
    `npm init -y`
    `-y` uses all the default options and fast forwards the npm init command

6. Install npm dependencies using,
    `npm i -D webpack webpack-cli`
    *One more thing we can do is add **node_modules** folder in the **.gitignore** file and you can also refer to django and python ignoring files by visiting [gitignore.io](gitignore.io) and add those in the **.gitignore** file.*

7. Install some more dependencies using npm,
    ```
    npm i -D @babel/core babel-loader @babel/preset-env @babel/preset-react babel-plugin-transform-class-properties
    ```

    `@babel/core` for only babel core package
    `babel-loader` for [transpiling](https://www.quora.com/What-is-a-transpiler) our code
    `@babel/preset-env` for compiling newer javascript code (eg. ES 2015, etc.)
    `babel-plugin-transform-class-properties` is for handling static class properties for ES 2015.

8. Also install the react itself,
    ```
    npm i -s react react-dom prop-types
    ```

9. Now we need to create two files, first for **babel** and second for **webpack**
    1. Create a file named **.babelrc** for babel configs.
    2. Create a file named **webpack.config.js** for webpack configs.

10. Add some scripts for development and production builds. Inside **package.json** file in the `"scripts"` key, remove `"test"` command and two new commands,
    ```
    "scripts": {
        "dev": "webpack --mode development ./leadmanager/frontend/src/index.js --output ./leadmanager/frontend/static/frontend/main.js",
        "build": "webpack --mode production ./leadmanager/frontend/src/index.js --output ./leadmanager/frontend/static/frontend/main.js","
    },
    ```

Checkout **leadmanager/frontend/src/index.js** and **leadmanager/frontend/src/components/App.js** for some basic react code which just renders a `h1` tag using the **index.html**
file inside **leadmanager/frontend/templates/** with `<div id="app"></div>` which
basically renders our react component.

You also need to run the `npm run dev` command for creating the **leadmanager/frontend/static/frontend/main.js** which is being loaded in the **index.html** mentioned above.

# Webpack Watch
Now after making changes in the **index.js** file we need to run `npm run dev` and then check for the new changes in the browser, so avoid running the `npm run dev` command we can just use the `--watch` option in *webpack* by adding it in the `dev` script.

## Notes
Suppose in a **DELETE** request, we want to remove the item from the list, in that case
we can use `.filter` method of JS array,

```
    case DELETE_LEAD:
      return {
        ...state,
        items: state.items.filter(item => item.id !== payload)
      }
```

`.filter` returns the updated list and in our case we are returning only those
items from the `state.items` whose `id` don't match the payload since our payload
contains the deleted item's id.

When appending or adding another object in a list we can either make use of array
methods like, `unshift`, `append`,

```
    case CREATE_LEAD:
      state.items.unshift(payload)
      return {
        ...state,
        items: state
      }
    ...
```

Or we can also use the ES6 spread operator,

```
    case CREATE_LEAD:
      return {
        ...state,
        items: [payload, ...state.items]
      }
    ...
```

----------------------------------------------------------------------------------------

**To get the exact error messages from the HTTP requests, we can use,**

```
    .catch(err => console.error(err.response.data))
```

`err.response.data` gives us an array containing the error messages that come
from our Django backend.

To get the *response* status we can use, `err.response.status`

-----------------------------------------------------------------------------------------

**django-rest-knox** has predefined views which can be included in our project, for
example we have `LogoutView` which logs the user out from the backend by deleting the
token of the user from the database, this way until the user not requests a new token by
login, the previous token in invalid.

```
from knox import views as knox_views

...
    path('api/auth/logout/', knox_views.LogoutView.as_view(), name='logout'),
...
```

`LogoutView` endpoint needs a **POST** request with `Authorization` header with
the user's token.

```
import requests
token = '<previous_token>'
res = requests.post('http://127.0.0.1:8000/api/auth/logout/', headers={'Authorization': f'Token {token}'})
res.status_code == 204
```

This way we can log the user out instead of just deleting the token from the frontend
storage options like localstorage or cookies.

-----------------------------------------------------------------------------------------

For creating private routes we are using a proxy function that will take the component
just like `Route` and `auth` state,

**src/components/common/privateRoutes.js**
```
import React from 'react';

// props are being passed to this functional component
const PrivateRoute = ({ component: Component, auth, ...rest }) => {

}
```

`...rest` is just like using `*args, **kwargs` in python, it takes in the rest of any
other arguments(props) that are being passed to our functional based component.
